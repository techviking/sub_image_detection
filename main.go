package main

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
)

var (
	source, sub image.Image
)

func main() {
	var (
		srcErr error
		subErr error
	)

	sourceFile, _ := os.Open("./assets/source.png")
	subImageFile, _ := os.Open("./assets/subset.png")


	source, srcErr = png.Decode(sourceFile)
	sub, subErr = png.Decode(subImageFile)

	if srcErr != nil {
		panic(fmt.Sprintf("srcErr: %s", srcErr))
	}

	if subErr != nil {
		panic(fmt.Sprintf("subErr: %s", subErr))
	}

	fmt.Printf("Size of source image is: %+v by %+v \n", source.Bounds().Max.X, source.Bounds().Max.Y)
	fmt.Printf("Size of sub image is: %+v by %+v \n", sub.Bounds().Max.X, sub.Bounds().Max.Y)

	sourceImageDX := source.Bounds().Max.X
	sourceImageDY := source.Bounds().Max.Y

	subImageDX := sub.Bounds().Max.X
	subImageDY := sub.Bounds().Max.Y

	//subImageTopLeftColor
	testColor := sub.At(0,0)
	subR, subG, subB, subA := testColor.RGBA()


	for sourceTestCoordX := 0; sourceTestCoordX <= sourceImageDX - subImageDX; sourceTestCoordX++ {
		for sourceTestCoordY := 0; sourceTestCoordY <= sourceImageDY - subImageDY; sourceTestCoordY++ {
			srcColor := source.At(sourceTestCoordX, sourceTestCoordY)

			srcR, srcG, srcB, srcA := srcColor.RGBA()

			if srcR == subR && srcG == subG && srcB == subB && srcA == subA {
				if fullCheck(sourceTestCoordX, sourceTestCoordY) {
					fmt.Println("SUB IMAGE FOUND!!! WOOOOOO!")
					fmt.Printf("coords on source image: %d, %d\n", sourceTestCoordX, sourceTestCoordY)
					writeNewImage(sourceTestCoordX, sourceTestCoordY)
					return
				}
			}
		}
	}

	panic("Sub image not found.  bleh.")
}

func fullCheck(minX, minY int) bool {
	for i := 0; i < sub.Bounds().Max.X; i++ {
		for j := 0; j < sub.Bounds().Max.Y; j++ {
			subColor := sub.At(i, j)
			srcColor := source.At(i + minX, j + minY)

			subR, subG, subB, subA := subColor.RGBA()
			srcR, srcG, srcB, srcA := srcColor.RGBA()

			if subR != srcR || subG != srcG || subB != srcB || subA != srcA {
				return false
			}
		}
	}

	return true
}

func writeNewImage(minX, minY int) {
	subImage := image.NewRGBA(sub.Bounds())

	for i := 0; i < sub.Bounds().Max.X; i++ {
		for j := 0; j < sub.Bounds().Max.Y; j++ {
			subImage.Set(i, j, source.At(minX + i, minY + j))
		}
	}

	data := subImage.Pix

	ioutil.WriteFile("./derp.png", data, os.ModePerm)
}

